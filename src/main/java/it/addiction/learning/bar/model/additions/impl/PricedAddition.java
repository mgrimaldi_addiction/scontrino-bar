package it.addiction.learning.bar.model.additions.impl;

import it.addiction.learning.bar.model.beverages.Beverage;

import java.math.BigDecimal;

/**
 * Created by Mario Grimaldi <m.grimaldi@addiction.it> with <3 on 19/06/15.
 */
public class PricedAddition extends BaseAddition {

    private final BigDecimal price;

    public PricedAddition(String name, BigDecimal price) {
        super(name);
        this.price = price;
    }

    public Beverage modifiedDrink(Beverage beverage) {
        return new Beverage.Builder(beverage)
                .withPrice(beverage.getBasePrice().add(this.price))
                .build();
    }
}
