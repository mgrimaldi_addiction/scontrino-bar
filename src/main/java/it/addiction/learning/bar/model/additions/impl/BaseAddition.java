package it.addiction.learning.bar.model.additions.impl;

import it.addiction.learning.bar.model.additions.Addition;

/**
 * Created by Mario Grimaldi <m.grimaldi@addiction.it> with <3 on 19/06/15.
 */
public abstract class BaseAddition implements Addition {

    private final String name;


    public BaseAddition(String name) {
        this.name = name;
    }
}
