package it.addiction.learning.bar.model;

import it.addiction.learning.bar.model.beverages.Beverage;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mario Grimaldi <m.grimaldi@addiction.it> with <3 on 19/06/15.
 */
public class Order {

    private List<Beverage> beverages = new ArrayList<Beverage>();

    public void addBeverage(Beverage beverage) {
        this.beverages.add(beverage);
    }

    public BigDecimal total() {
        BigDecimal total = new BigDecimal(0);

        for (Beverage beverage : this.beverages) {
            total = total.add(beverage.getPrice());
        }

        return total;
    }

}
