package it.addiction.learning.bar.model.additions.impl;

import it.addiction.learning.bar.model.beverages.Beverage;

import java.math.BigDecimal;

/**
 * Created by Mario Grimaldi <m.grimaldi@addiction.it> with <3 on 19/06/15.
 */
public class PercentageAddition extends BaseAddition {

    private final BigDecimal percent;

    public PercentageAddition(String name, BigDecimal percent) {
        super(name);
        this.percent = percent;
    }

    public Beverage modifiedDrink(Beverage beverage) {
        BigDecimal priceToAdd = beverage.getBasePrice().multiply(this.percent);
        return new Beverage.Builder(beverage)
                .withPrice(beverage.getBasePrice().add(priceToAdd))
                .build();
    }
}
