package it.addiction.learning.bar.model.additions;

import it.addiction.learning.bar.model.beverages.Beverage;

/**
 * Created by Mario Grimaldi <m.grimaldi@addiction.it> with <3 on 19/06/15.
 */
public interface Addition {

    Beverage modifiedDrink(Beverage beverage);

}
