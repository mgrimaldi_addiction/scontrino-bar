package it.addiction.learning.bar.model.additions.factory;

import it.addiction.learning.bar.model.additions.Addition;
import it.addiction.learning.bar.model.additions.impl.BaseAddition;
import it.addiction.learning.bar.model.additions.impl.PercentageAddition;
import it.addiction.learning.bar.model.additions.impl.PricedAddition;
import it.addiction.learning.bar.model.beverages.Beverage;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mario Grimaldi <m.grimaldi@addiction.it> with <3 on 19/06/15.
 */
public class AdditionFactory {

    public static final String WHISKY = "whisky";
    public static final String RHUM = "rhum";
    public static final String GRAPPA = "grappa";
    public static final String TOPPING_PANNA = "topping_panna";
    public static final String TOPPING_CACAO = "topping_cacao";
    public static final String TOPPING_GRANELLA_NOCCIOLA = "topping_granella_nocciola";
    public static final String FONDO_NOCCIOLA = "fondo_nocciole";
    public static final String FONDO_PISTACCHIO = "fondo_pistacchio";
    public static final String FONDO_CIOCCOLATO = "fondo_cioccolato";

    private final Map<String, Addition> additions = new HashMap<String, Addition>() {{
        put(WHISKY, new PercentageAddition("Whisky", new BigDecimal(0.5)));
        put(RHUM, new PercentageAddition("Rhum", new BigDecimal(0.5)));
        put(GRAPPA, new PercentageAddition("Grappa", new BigDecimal(0.7)));

        put(TOPPING_PANNA, new PricedAddition("Topping Panna", new BigDecimal(0.1)));
        put(TOPPING_CACAO, new PricedAddition("Topping Cacao", new BigDecimal(0.2)));
        put(TOPPING_GRANELLA_NOCCIOLA, new PricedAddition("Topping Granella di Nocciole", new BigDecimal(0.3)));

        put(FONDO_NOCCIOLA, new BaseAddition("Fondo Nocciole") {
            public Beverage modifiedDrink(Beverage beverage) {
                final BigDecimal additionPrice = beverage.getBasePrice().compareTo(new BigDecimal(1)) == 1 ? new BigDecimal(0.5) : new BigDecimal(0);
                return new Beverage.Builder(beverage)
                        .withPrice(beverage.getBasePrice().add(additionPrice))
                        .build();
            }
        });
        put(FONDO_PISTACCHIO, new BaseAddition("Fondo Pistacchio") {
            public Beverage modifiedDrink(Beverage beverage) {
                final BigDecimal additionPrice = beverage.getBasePrice().compareTo(new BigDecimal(1)) == 1 ? new BigDecimal(0.5) : new BigDecimal(0);
                return new Beverage.Builder(beverage)
                        .withPrice(beverage.getBasePrice().add(additionPrice))
                        .build();
            }
        });
        put(FONDO_CIOCCOLATO, new BaseAddition("Fondo Cioccolato") {
            public Beverage modifiedDrink(Beverage beverage) {
                final BigDecimal additionPrice = beverage.getBasePrice().compareTo(new BigDecimal(1)) == 1 ? new BigDecimal(0.3) : new BigDecimal(0);
                return new Beverage.Builder(beverage)
                        .withPrice(beverage.getBasePrice().add(additionPrice))
                        .build();
            }
        });
    }};


    public Addition getAddition(String name) {
        Addition addition = this.additions.get(name);

        if (addition == null) throw new RuntimeException("L'aggiunta specificata non esiste.");

        return addition;
    }
}
