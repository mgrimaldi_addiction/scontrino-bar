package it.addiction.learning.bar.model.beverages;

import it.addiction.learning.bar.model.additions.Addition;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Mario Grimaldi <m.grimaldi@addiction.it> with <3 on 19/06/15.
 */
public class Beverage {

    private final String name;
    private final BigDecimal price;
    private List<Addition> additions = new ArrayList<Addition>();

    private Beverage(String name, BigDecimal price, List<Addition> additions) {
        this.name = name;
        this.price = price;
        this.additions = new ArrayList<Addition>(additions);
    }

    public String getName() {
        return name;
    }

    public BigDecimal getBasePrice() {
        return this.price;
    }

    public BigDecimal getPrice() {
        Beverage modifiedBeverage = new Beverage(this.getName(), this.getBasePrice(), this.getAdditions());
        for (Addition addition : this.additions) {
            modifiedBeverage = addition.modifiedDrink(modifiedBeverage);
        }
        return modifiedBeverage.getBasePrice();
    }

    public List<Addition> getAdditions() {
        return Collections.unmodifiableList(this.additions);
    }

    public static class Builder {
        private String name;
        private BigDecimal price;
        private List<Addition> additions = new ArrayList<Addition>();


        public Builder(String name, BigDecimal price) {
            this.name = name;
            this.price = price;
        }

        public Builder(Beverage beverage) {
            this.name = beverage.getName();
            this.price = beverage.getBasePrice();
            this.additions = new ArrayList<Addition>(beverage.getAdditions());
        }

        public Builder withPrice(BigDecimal price) {
            this.price = price;
            return this;
        }

        public Builder withAddition(Addition addition) {
            this.additions.add(addition);
            return this;
        }

        public Beverage build() {
            return new Beverage(this.name, this.price, this.additions);
        }
    }

}
