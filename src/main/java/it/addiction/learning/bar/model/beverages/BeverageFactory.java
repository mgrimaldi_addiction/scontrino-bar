package it.addiction.learning.bar.model.beverages;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mario Grimaldi <m.grimaldi@addiction.it> with <3 on 19/06/15.
 */
public class BeverageFactory {

    public static final String CAFFE = "caffe";
    public static final String THE = "the";
    public static final String KARKADE = "karkade";

    private Map<String, Beverage.Builder> beverages = new HashMap<String, Beverage.Builder>() {{
        put(CAFFE, new Beverage.Builder("Caffè", new BigDecimal(1)));
        put(THE, new Beverage.Builder("Thè", new BigDecimal(2)));
        put(KARKADE, new Beverage.Builder("Karkadè", new BigDecimal(3)));
    }};

    public Beverage.Builder getBeverage(String name) {
        Beverage.Builder beverageBuilder = this.beverages.get(name);

        if (beverageBuilder == null) throw new RuntimeException("La bevanda specificata non è in menu.");

        return beverageBuilder;
    }

}
