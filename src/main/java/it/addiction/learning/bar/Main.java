package it.addiction.learning.bar;

import it.addiction.learning.bar.model.additions.Addition;
import it.addiction.learning.bar.model.additions.factory.AdditionFactory;
import it.addiction.learning.bar.model.beverages.Beverage;
import it.addiction.learning.bar.model.Order;
import it.addiction.learning.bar.model.beverages.BeverageFactory;

import java.math.BigDecimal;

/**
 * Created by Mario Grimaldi <m.grimaldi@addiction.it> with <3 on 19/06/15.
 */
public class Main {

    public static void main(String[] args) {
        BeverageFactory beverageFactory = new BeverageFactory();
        AdditionFactory additionFactory = new AdditionFactory();

        Order order = new Order();

        Beverage caffeParticolare = beverageFactory.getBeverage(BeverageFactory.CAFFE)
                .withAddition(additionFactory.getAddition(AdditionFactory.FONDO_NOCCIOLA))
                .withAddition(additionFactory.getAddition(AdditionFactory.WHISKY))
                .withAddition(additionFactory.getAddition(AdditionFactory.TOPPING_CACAO))
                .build();

        order.addBeverage(caffeParticolare);

        System.out.println(order.total());
    }

}
